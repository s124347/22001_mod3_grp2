%% Measurement 
%
%


data_entries = dir('data/*.mat');

N_entries = length(data_entries);

% Declare value vectors
Index               = (1:N_entries)';
TimeCode            = cell(N_entries, 1);
SamplingFrequency   = zeros(N_entries, 1);
FFT_Size             = zeros(N_entries, 1);
PsedoNoiseSize      = zeros(N_entries, 1);
TotalSize           = zeros(N_entries, 1);
Repetitions         = zeros(N_entries, 1);
Overlap             = zeros(N_entries, 1);
NoiseType           = cell(N_entries, 1);
WindowType          = cell(N_entries, 1);

% Loop through all found data entries
for entry_idx = 1:N_entries
    
    data_object = load( fullfile(data_entries(entry_idx).folder, data_entries(entry_idx).name) );
    
    time_code_temp = strsplit(data_entries(entry_idx).name, '_Real');
    
    TimeCode(entry_idx) = time_code_temp(1);
    SamplingFrequency(entry_idx) = data_object.fs;
    FFT_Size(entry_idx) = data_object.N_stft;
    Overlap(entry_idx) = data_object.overlap;
    PsedoNoiseSize(entry_idx) = data_object.N_prn;
    TotalSize(entry_idx) = data_object.N_total;
    Repetitions(entry_idx) = data_object.nRepetitions;
    NoiseType(entry_idx) = {data_object.noiseType};
    WindowType(entry_idx) = {data_object.winType};
    
end

data_table = table(Index, ...
                   TimeCode, ...
                   SamplingFrequency, ...
                   FFT_Size, ...
                   PsedoNoiseSize, ...
                   TotalSize, ...
                   Repetitions, ...
                   Overlap, ...
                   NoiseType, ...
                   WindowType)
               
               
%%
data_table
data_table([8 10 11 12],:)

               