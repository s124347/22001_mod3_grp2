function apply_axis_params(ax)
    if nargin < 1
        ax = gca;
    end
    
    params = evalin('base', "params");
    fields = fieldnames(params.axis);
    
    for p_idx = 1:length(fields)
        try
            set(ax, fields{p_idx}, params.axis.(fields{p_idx}));
        catch e
            warning(e.message)
        end
    end
end