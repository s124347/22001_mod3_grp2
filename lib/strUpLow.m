function str = strUpLow(str)
%% strUpLow(str)
%
%   Reference: https://se.mathworks.com/matlabcentral/answers/107307-function-to-capitalize-first-letter-in-each-word-in-string-but-forces-all-other-letters-to-be-lowerc
%
%   Regexp explained:
%       '(?<=\s+) means all words beginning with white space
%       \S' , means flowed with any non white space character
%       'start')-1; gives the index of the beginning of each word, I added -1 because I've used [' ' str];
%
str=lower(str);
idx=regexp([' ' str],'(?<=\s+)\S','start')-1;
str(idx)=upper(str(idx));