function save_figure(path, w, h, varargin)
% SAVE_FIGURE Save the current figure (gcf)
% 
% SAVE_FIGURE does stuff (write description)
%
% SAVE_FIGURE(path,w,h,__) saves the figure to path with given sizes 
%
% SAVE_FIGURE(__,'dryrun',trueOrFalse) runs the function but skips the
% actual writing to disk part
%
    if nargin > 3
        [varargin{:}] = convertStringsToChars(varargin{:});
    end
    
    % Load boolean/flag options
    [dryrun, varargin] = getFlagOption(varargin, 'dryrun');
    [tighten, ~] = getFlagOption(varargin, 'tight');

    fig = gcf;
    children = fig.Children;
    for ax_idx = 1:length(children)
        ax = children(ax_idx);
        if strcmpi(ax.Type,'axes') && tighten
            outerpos = ax.OuterPosition;
            ti = ax.TightInset; 
            left = outerpos(1) + ti(1);
            bottom = outerpos(2) + ti(2);
            ax_width = outerpos(3) - ti(1) - ti(3);
            ax_height = outerpos(4) - ti(2) - ti(4);
            ax.Position = [left bottom ax_width ax_height];
        end
        % Apply global axis parameters
        apply_axis_params(ax);
    end
    
    % Apply global figure parameters
    apply_fig_params(fig);

    set_fig_size(fig, w, h);
    % Jump out if dryrun is needed
    if dryrun
        return
    end

    % Save in a bunch of formats
    % saveas(gcf,path,'epsc')
    % saveas(gcf,path,'png')
    fprintf('Attempting to save gcf to:\n\t%s\n', path);
    if exist(fullfile(pwd, strcat(path, '.pdf')),'file')
        delete(fullfile(pwd, strcat(path, '.pdf')))
    end
    print(gcf, path, '-dpdf', '-fillpage')
    
    if exist(fullfile(pwd, strcat(path,'.pdf')),'file')
        fprintf('Figure saved.\n');
    else
        fprintf('Figure not saved.\n');
    end
end


% -------------------------------------------------------------------------
function [f,arg] = getFlagOption(arg, flag)
    f = false;

    i = 1;
    while i <= numel(arg)
        if ischar(arg{i}) && strncmpi(arg{i},flag,2)
            f = arg{i+1};
            arg([i i+1])=[];
        else
            i = i+1;
        end
    end
end
