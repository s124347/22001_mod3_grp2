function set_xyz_props(axis, axis_letter, varargin)
%% set_xyz_props(axis_letter,varargin)
%
%   set_xyz_props(axis_letter,__) does nothing
%
%   set_xyz_props(__,'lim', limit)
%       sets limit, e.g. ylim
%   set_xyz_props(__,'tick', ticks)
%       sets ticks, e.g. yticks
%   set_xyz_props(__,'ticklabel', ticklabels)
%       sets ticklabels, e.g. yticklabels
%   set_xyz_props(__,'label', label)
%       sets label, e.g. ylabel
%
    if nargin > 1
        [varargin{:}] = convertStringsToChars(varargin{:});
    end
    
    varargin = applyOption(axis, varargin, 'lim');
    varargin = applyOption(axis, varargin, 'tick');
    varargin = applyOption(axis, varargin, 'ticklabel');
    varargin = applyLabelString(axis, varargin); % label
    varargin = applyOption(axis, varargin, 'scale');
        
    
% -------------------------------------------------------------------------
function [arg] = applyOption(axis, arg, flag)
    % Early return if input varargin is empty, or free from name-value
    % pairs
    if numel(arg) < 2
        return
    end
    
    i = 1;
    
    while i <= numel(arg)
        if ischar(arg{i}) && strcmpi(arg{i},flag)
            set(axis, strcat(axis_letter, flag), arg{i+1});
            arg([i i+1])=[];
            return
        else
            i = i+1;
        end
    end
end

function [arg] = applyLabelString(axis, arg)
    % Early return if input varargin is empty, or free from name-value
    % pairs
    if numel(arg) < 2
        return
    end
    
    i = 1;
    
    while i <= numel(arg)
        if ischar(arg{i}) && strcmpi(arg{i},'label')
            switch lower(axis_letter)
                case 'x'
                    xlabel(axis, arg{i+1});
                case 'y'
                    ylabel(axis, arg{i+1});
                case 'z'
                    zlabel(axis, arg{i+1});
            end
            arg([i i+1])=[];
            return
        else
            i = i+1;
        end
    end
end

end

