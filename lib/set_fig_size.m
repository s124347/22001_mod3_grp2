function set_fig_size(fig, w, h, unit)
%SET_FIG_SIZE Set size of figure in unit
%
%   f:      figure handle
%   h:      height in  'unit'
%   w:      width in 'unit'
%   unit:   defaults to centimeters
%
    if isempty(fig)
        fig = gcf;
    end
    
    if nargin < 4 || isempty(unit)
        unit = 'centimeters';
    end

    set(fig,'Units', unit);
    oldPos = get(fig, 'Position');
    set(fig,'Position', [oldPos(1) oldPos(2) w h]);
    newPos = get(fig, 'Position');
    set(fig,'PaperPositionMode','Auto','PaperUnits',unit,'PaperSize',[newPos(3), newPos(4)])
end