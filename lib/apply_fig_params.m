function apply_fig_params(fig)
    if nargin < 1
        fig = gcf;
    end
    
    params = evalin('base', "params");
    fields = fieldnames(params.figure);
    
    for p_idx = 1:length(fields)
        try
            set(fig, fields{p_idx}, params.figure.(fields{p_idx}));
        catch e
            warning(e.message)
        end
    end
end