%% Analysis of noisetype and windowtype
%
%

% Get measurement files
%
data_entries = dir('data/*.mat');

% 'Correctly combined' and 'non ideal combined' measurement indices
%
idx_correctly_combined = [3 7];
idx_incorrectly_combined = [5 6];


%% Correctly combined vs Incorrectly combined
figure()
%xl = [1.225e3 1.775e3];
xl = [900 2.5e3]
yl = [-60 30];
yt = yl(1):10:yl(2);
xt = [250 500 600 700 800 900 1e3 1.5e3 2e3 3e3 4e3 5e3 6e3 8e3 10e3 12e3];
%xt = [1e3:5e1:xl(2)];
ts_nW = 2;
ts_nH = 2;
ts_gap = [0.025 .01];
ts_marg_h = [0.11 .025];
ts_marg_w = [.05 0.0125];
ha = tight_subplot(ts_nH, ts_nW, ts_gap, ts_marg_h, ts_marg_w);

ts_i = 1;
for entry_idx = [idx_correctly_combined idx_incorrectly_combined]
    data_object = load( fullfile(data_entries(entry_idx).folder, data_entries(entry_idx).name) );
    
    hold(ha(ts_i),'on')
    Cplot = plot(ha(ts_i), data_object.f, mag2db(abs(data_object.XY ./ data_object.XX)));
    Cplot.LineWidth = params.plot.linewidth.normal;
    Cplot.Color = params.plot.colormap(1,:);
    H2 = plot(ha(ts_i), data_object.f, mag2db(abs(data_object.YY ./ conj(data_object.XY))));
    H2.LineWidth = params.plot.linewidth.normal;
    H2.Color = params.plot.colormap(2,:);
    
    if mod(ts_i,2) == 1
        set_xyz_props(ha(ts_i), 'y', ...
                      'lim',        yl, ...
                      'tick',       yt, ...
                      'ticklabel',  {'' yt(2:end-1) ''}, ...
                      'scale',      'linear', ...
                      'label',      'dB');
    else
        set_xyz_props(ha(ts_i), 'y', ...
                      'lim',        yl, ...
                      'tick',       yt, ...
                      'ticklabel',  [], ...
                      'scale',      'linear', ...
                      'label',      '');
    end
    if ts_i > 2
        set_xyz_props(ha(ts_i), 'x', ...
                      'lim',        xl, ...
                      'tick',       xt, ...
                      'ticklabel',  xt * 1e-3, ...
                      'scale',      'log', ...
                      'label',      'Frequency [kHz]');
    else
        set_xyz_props(ha(ts_i), 'x', ...
                      'lim',        xl, ...
                      'tick',       xt, ...
                      'ticklabel',  [], ...
                      'scale',      'log', ...
                      'label',      'Frequency [kHz]');
    end
    
    text(ha(ts_i),...
         xl(1) + 25, yl(2) - 2.5, ...
         sprintf('%s %s', data_object.noiseType, data_object.winType), ...
         'BackGroundColor', 'w', ...
         'FontSize',params.plot.fontsize, ...
         'HorizontalAlignment', 'left', ...
         'VerticalAlignment', 'top');
    apply_axis_params()
    
    % Increment axis index
    ts_i = ts_i + 1;
end

leg = legend(ha(end),[Cplot H2],{'S_{XY} / S_{XX}', 'S_{YY} / S_{XY}^*'});
leg.Location = 'southwest';
leg.FontSize = params.axis.fontsize;

save_figure('figures/TF_2good_2bad.pdf', 18.1 * params.plot.scalefactor, 6 * params.plot.scalefactor, 'dryrun', false)


% Same as above, but only coherence
figure()
xl = [750 1.5e3];
yl = [0 1];
yt = yl(1):.1:yl(2);
xt = [250 500 750 1e3 2e3 4e3 6e3 8e3 10e3 12e3];

ts_nW = 1;
ts_nH = 2;
ts_gap = [0.025 .025];
ts_marg_h = [0.11 .025];
ts_marg_w = [.1 0.0125];
ha = tight_subplot(ts_nH, ts_nW, ts_gap, ts_marg_h, ts_marg_w);

leg_str = cell(2,1);

ts_i = 1;
hold(ha(ts_i),'on')
for ii = 1:2
    % Use ideally matched parameters
    entry_idx = idx_correctly_combined(ii);
    
    % Load data
    data_object = load( fullfile(data_entries(entry_idx).folder, data_entries(entry_idx).name) );

    % Plot
    Cplot(ii) = plot(ha(ts_i), data_object.f, (abs(data_object.C)));
    Cplot(ii).LineWidth = params.plot.linewidth.normal;
    Cplot(ii).Color = params.plot.colormap(ii,:);
    
    % Save parameters for legend string
    leg_str{ii} = strUpLow(sprintf('%s-noise & %s-window', data_object.noiseType, data_object.winType));
end
hold(ha(ts_i),'off')

set_xyz_props(ha(ts_i), 'y', ...
              'lim',        yl, ...
              'tick',       yt, ...
              'ticklabel',  {'' yt(2:end)}, ...
              'scale',      'linear', ...
              'label',      'Coherence');

set_xyz_props(ha(ts_i), 'x', ...
              'lim',        xl, ...
              'tick',       xt, ...
              'ticklabel',  [], ...
              'scale',      'log', ...
              'label',      'Frequency [kHz]');

leg = legend(ha(ts_i),[Cplot(1) Cplot(2)],leg_str);
leg.Location = 'southeast';
leg.FontSize = params.axis.fontsize;

ts_i = 2;
hold(ha(ts_i),'on')
for ii = 1:2
    % Use non ideally matched parameters
    entry_idx = idx_incorrectly_combined(ii);
    
    % Load data
    data_object = load( fullfile(data_entries(entry_idx).folder, data_entries(entry_idx).name) );

    % Plot
    Cplot(ii) = plot(ha(ts_i), data_object.f, (abs(data_object.C)));
    Cplot(ii).LineWidth = params.plot.linewidth.normal;
    Cplot(ii).Color = params.plot.colormap(ii,:);
    
    % Save parameters for legend string
    leg_str{ii} = strUpLow(sprintf('%s-noise & %s-window', data_object.noiseType, data_object.winType));
end
hold(ha(ts_i),'off')

set_xyz_props(ha(ts_i), 'y', ...
              'lim',        yl, ...
              'tick',       yt, ...
              'ticklabel',  yt, ...
              'scale',      'linear', ...
              'label',      'Coherence');
set_xyz_props(ha(ts_i), 'x', ...
              'lim',        xl, ...
              'tick',       xt, ...
              'ticklabel',  xt * 1e-3, ...
              'scale',      'log', ...
              'label',      'Frequency [kHz]');

leg = legend(ha(ts_i),[Cplot(1) Cplot(2)],leg_str);
leg.Location = 'southeast';
leg.FontSize = params.axis.fontsize;

save_figure('figures/coherence_right_vs_wrong.pdf', 8.8 * params.plot.scalefactor, 6 * params.plot.scalefactor, 'dryrun', false)
