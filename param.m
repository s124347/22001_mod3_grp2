%%

params = struct();
% Flag for saving plots (should be true if the newest plots should be
% copied as well)
params.plot.save.yesNo = true;
% All plots in would be saved with the extension
params.plot.save.ext = '.pdf';
% All plots will be saved to the same location and named after the script
% file
params.plot.save.location = 'figures';

% Flag for enabling copying of plots to desired destination
params.plot.copy.yesNo = false;
% All files in the figure folder with the extension:
params.plot.copy.ext = '.pdf';
% are then copied to the destination following destination:
% (usually that's the same location as where you save them)
params.plot.copy.ocation = '';

% Get 5-color colormap using colorbrewer2 (external dependency)
params.plot.colormap = cbrewer('qual','Set1',5);

% Global figure parameters
%   The parameters named:
%       param_fig_*
%
%   These will be applied to all figures, thus they have to be named as a
%   parameter for figure
%
params.figure.color = 'w';
params.figure.fontsize = 12;

% Global axis parameters
%   param_axis_*
%
%   These will be applied to all axes, thus they have to be named as a
%   parameter for axes
%
params.axis.linewidth = 1.5;
params.axis.fontsize = 12;
params.axis.xgrid = 'on';
params.axis.ygrid = 'on';
params.axis.box = 'on';

% Global plot parameters
%   param.plot.*
params.plot.linewidth.hair   = .25;
params.plot.linewidth.thin   = 1.0;
params.plot.linewidth.normal = 1.5;
params.plot.linewidth.bold   = 2.5;
params.plot.fontsize = 12;
params.plot.scalefactor = 2;
