%% RUN TESTS
%
%   Creates a suite from all the test classes in tests and executes them
%
import matlab.unittest.TestSuite
suiteFolder = TestSuite.fromFolder('tests');
result = run(suiteFolder);