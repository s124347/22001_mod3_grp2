%% Load noise recording
%
%


data_entries = dir('data/*.mat');


data_object1 = load(fullfile(data_entries(4).folder,data_entries(4).name));
data_object2 = load(fullfile(data_entries(4).folder,data_entries(5).name));


% -- figure
figure()

% subplot setup
ts_nW = 1;
ts_nH = 2;
ts_gap = [0.1 .025];
ts_marg_h = [0.2 .015];
ts_marg_w = [.1 0.0175];

% create subplot
ha = tight_subplot(ts_nH, ts_nW, ts_gap, ts_marg_h, ts_marg_w);

% -- axis parameters
xl = [1.5 3.5];
xt = xl(1):(xl(2)-xl(1))/20:xl(2);
yl = [-.125 .125];
yt = -.1:.05:.1;

% plot subplot 1
axes(ha(1));
hold on
y1 = plot(data_object1.t, data_object1.y(:,1));
y2 = plot(data_object1.t, data_object1.y(:,2));
hold off

y1.LineWidth = params.plot.linewidth.normal;
y1.Color = params.plot.colormap(1,:);

y2.LineWidth = params.plot.linewidth.normal;
y2.Color = params.plot.colormap(2,:);

set_xyz_props(ha(1), 'y', ...
              'lim',        yl, ...
              'tick',       yt, ...
              'ticklabel',  yt, ...
              'scale',      'linear', ...
              'label',      'Level [a.u.]');

set_xyz_props(ha(1), 'x', ...
              'lim',        xl, ...
              'tick',       xt, ...
              'ticklabel',  [], ...
              'label',      '');

% plot subplot 2
axes(ha(2));
hold on
y1 = plot(data_object2.t, data_object2.y(:,1));
y2 = plot(data_object2.t, data_object2.y(:,2));
hold off

y1.LineWidth = params.plot.linewidth.hair;
y1.Color = params.plot.colormap(1,:);

y2.LineWidth = params.plot.linewidth.hair;
y2.Color = params.plot.colormap(2,:);

set_xyz_props(ha(2), 'y', ...
              'lim',        yl, ...
              'tick',       yt, ...
              'ticklabel',  yt, ...
              'scale',      'linear', ...
              'label',      'Level [a.u.]');

set_xyz_props(ha(2), 'x', ...
              'lim',        xl, ...
              'tick',       xt, ...
              'ticklabel',  xt, ...
              'label',      'Time [s]');
          
save_figure('figures/backgroundnoise.pdf', 8.8 * params.plot.scalefactor, 4 * params.plot.scalefactor, 'dryrun', false)