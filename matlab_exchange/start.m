%% Matlab exchange startup script
%
%

%% CBREWER
%
%   cbrewer : colorbrewer schemes for Matlab
%
%   https://se.mathworks.com/matlabcentral/fileexchange/34087-cbrewer-colorbrewer-schemes-for-matlab
%
addpath(fullfile(fileparts(which(mfilename)),'cbrewer'));


%% TIGHT_SUBPLOT
%
%   tight_subplot : 
%
%   [link]
%
addpath(fullfile(fileparts(which(mfilename)),'tight_subplot'));

