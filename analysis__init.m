%% Init analysis of data
%
%   Basically load it and investigate the measurements
%
%   Also, this analysis script prints the settings used for each
%   measurement. Which ideally would be used for quick reference etc
%

data_entries = dir('data/*.mat');


%% Preliminary plot of transfer functions H1 and H2
figure()
ha = tight_subplot(length(data_entries),1,[0 .1],0,0,true);

for entry_idx = 1:length(data_entries)
    
    data_object = load( fullfile(data_entries(entry_idx).folder, data_entries(entry_idx).name) );
    
    hold on
    plot(ha(entry_idx), data_object.f, mag2db(abs(data_object.H1)));
    plot(ha(entry_idx), data_object.f, mag2db(abs(data_object.H2)));
    
    xlim([250 12e3]);
    hold off
    
end

%% Preliminary plot of coherence H1 and H2


%% Measure 1 to 10
figure()
xl = [150 13e3];
yl = [-60 30];
yt = yl(1):10:yl(2);
xt = [250 500 750 1e3 2e3 4e3 6e3 8e3 10e3 12e3];

ts_nW = 2;
ts_nH = 5;
ts_gap = [0 .025];
ts_marg_h = [0 .025];
ts_marg_w = [.025 0];
ha = tight_subplot(ts_nH, ts_nW, ts_gap, ts_marg_h, ts_marg_w);

for entry_idx = 1:10
    data_object = load( fullfile(data_entries(entry_idx).folder, data_entries(entry_idx).name) );
    
    hold(ha(entry_idx),'on')
    H1 = plot(ha(entry_idx), data_object.f, mag2db(abs(data_object.XY ./ data_object.XX)));
    H2 = plot(ha(entry_idx), data_object.f, mag2db(abs(data_object.YY ./ conj(data_object.XY))));
    
    set_xyz_props(ha(entry_idx), 'y', ...
                  'lim',        yl, ...
                  'tick',       yt, ...
                  'ticklabel',  [], ...
                  'scale',      'linear', ...
                  'label',      'dB');
                       
    set_xyz_props(ha(entry_idx), 'x', ...
                  'lim',        xl, ...
                  'tick',       xt, ...
                  'ticklabel',  [], ...
                  'scale',      'log', ...
                  'label',      'Frequency [Hz]');
    
    text(ha(entry_idx),...
         150, 10, ...
         sprintf('%s rep: %d N_{prn}: %d N_{stft}: %d', ...
                 data_object.noiseType, ...
                                              data_object.nRepetitions, ...
                                              data_object.N_prn, ...
                                              data_object.N_stft), ...
         'BackgroundColor','w', ...
         'FontSize',params.plot.fontsize, ...
         'HorizontalAlignment', 'left', ...
         'VerticalAlignment', 'top');
    apply_axis_params()
end

legend(ha(end),[H1 H2],{'$C$', '$\frac{S_{XY}}{S_{XX}}$', '$\frac{S_{YY}}{S_{XY}^*}$'}, 'Interpreter', 'latex');

save_figure('Coherence_for_all.pdf', 36, 24, 'dryrun', true)


%% Measure 11 to 19
figure()
xl = [150 13e3];
yl = [-60 30];
yt = yl(1):10:yl(2);
xt = [250 500 750 1e3 2e3 4e3 6e3 8e3 10e3 12e3];

ts_nW = 2;
ts_nH = 5;
ts_gap = [0 .025];
ts_marg_h = [0 .025];
ts_marg_w = [.025 0];
ha = tight_subplot(ts_nH, ts_nW, ts_gap, ts_marg_h, ts_marg_w);

for entry_idx = 1:9
    data_object = load( fullfile(data_entries(entry_idx+10).folder, data_entries(entry_idx+10).name) );
    
    hold(ha(entry_idx),'on')
    H1 = plot(ha(entry_idx), data_object.f, mag2db(abs(data_object.XY ./ data_object.XX)));
    H2 = plot(ha(entry_idx), data_object.f, mag2db(abs(data_object.YY ./ conj(data_object.XY))));
    
    set_xyz_props(ha(entry_idx), 'y', ...
                  'lim',        yl, ...
                  'tick',       yt, ...
                  'ticklabel',  [], ...
                  'scale',      'linear', ...
                  'label',      'dB');
                       
    set_xyz_props(ha(entry_idx), 'x', ...
                  'lim',        xl, ...
                  'tick',       xt, ...
                  'ticklabel',  [], ...
                  'scale',      'log', ...
                  'label',      'Frequency [Hz]');
    
    text(ha(entry_idx),...
         xl(1) + 10, yl(2) - 2.5, ...
         sprintf('%s rep: %d N_{prn}: %d N_{stft}: %d, f_s: %d', ...
                 data_object.noiseType, ...
                 data_object.nRepetitions, ...
                 data_object.N_prn, ...
                 data_object.N_stft, ...
                 data_object.fs), ...
         'FontSize',params.plot.fontsize, ...
         'HorizontalAlignment', 'left', ...
         'VerticalAlignment', 'top');
    apply_axis_params()
end

legend(ha(end),[H1 H2],{'$C$', '$\frac{S_{XY}}{S_{XX}}$', '$\frac{S_{YY}}{S_{XY}^*}$'}, 'Interpreter', 'latex');

save_figure('Coherence_for_all.pdf', 36, 24, 'dryrun', true)