% %STARTUP 
%
%   This ensures all related scripts are included in the workspace prior to
%   running any of the analysis etc.

%% Add matlab-exchange functions
addpath matlab_exchange/
run matlab_exchange/start.m

%% Add JTL lib functions
addpath lib

%% Add own functions
addpath functions

%% Add data folder
addpath data

%% Add unit test folder
addpath tests

%% Set parameters
run param.m

